@extends('layouts.app')

@section('head')
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <h2 class="float-left">The latest 20 recipes</h2>
        <a class="btn btn-info float-right btn-lg" href="/export/recipes">Export all recipes</a>
        <div class="clearfix"></div>
        <div class="row justify-content-center">
            @foreach($recipes->reverse()->take(20) as $recipe)
                <div class="d-flex recipe-card m-2">
                    <a href="/recipe/{{ $recipe->id }}" class="recipe-card-link">
                        <div class="card card-home">
                            <img class="card-img-top" src="/uploads/recipes/{{ $recipe->image }}" alt="{{$recipe->name}}">
                            <div class="card-body">
                                <h5 class="card-title">{{ $recipe->name }}</h5>
                                <p class="card-text">
                                    {{ count(explode(' ', $recipe->instructions)) < 50 ? implode(' ', array_slice(explode(' ', $recipe->instructions), 0, 50)) : implode(' ', array_slice(explode(' ', $recipe->instructions), 0, 50)) . '...'}}</p>

                            </div>
                            <div class="card-footer">
                                <small class="text-muted">By {{ $recipe->user->name }}</small>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
            <a class="btn btn-success float-right btn-lg btn-new-recipe" href="/recipe/create">+</a>
        </div>
    </div>

@endsection
