<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/profile', 'ProfileController@index');
Route::get('/profile/{user}', 'ProfileController@show');
Route::get('/profile/{user}/edit', 'ProfileController@edit')->middleware('can:update,user');
Route::put('/profile/{user}', 'ProfileController@update')->middleware('can:update,user');


Route::get('/', 'RecipeController@index'); //READ
Route::get('/recipes', 'RecipeController@index'); //READ

Route::get('/recipe/create', 'RecipeController@create'); //CREATE
Route::post('/recipe', 'RecipeController@store'); //CREATE

Route::get('/recipe/{recipe}', 'RecipeController@show'); //READ

Route::get('/recipe/{recipe}/edit', 'RecipeController@edit')->middleware('can:update,recipe'); //UPDATE
Route::put('/recipe/{recipe}', 'RecipeController@update')->middleware('can:update,recipe'); //UPDATE

Route::delete('/recipe/{recipe}', 'RecipeController@destroy')->middleware('can:delete,recipe'); //DELETE


Route::get('/profile', 'ProfileController@index')->name('profile');

Route::get('/export/recipes','RecipeController@export');