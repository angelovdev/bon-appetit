<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Image;
use PDF;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $userId = Auth::user()->id;
        return redirect("profile/{$userId}");

    }
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }
    public function edit(User $user)
    {
        return view("users.edit", compact('user'));
    }
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        $filename = $user->avatar;

        if ($request->hasFile('avatar'))
        {
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->fit(300)
//            ->circle(70, 150, 100, function ($draw) {
//                $draw->border(5, '000000');
//            })
//
            ->insert(public_path('/uploads/watermark.png'), 'bottom-right', 10, 10)
            ->save(public_path('/uploads/avatars/') . $filename);
            $user->avatar = $filename;
            $user->save();
        }

        $user->update(request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
        ]));

        return redirect('/profile/' . $user->id);

    }
}
