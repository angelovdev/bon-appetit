<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Recipe;
use App\User;
use Faker\Generator as Faker;

$factory->define(Recipe::class, function (Faker $faker) {
    return [
        'user_id' => User::all()->isNotEmpty() ? User::all()->random()->id : factory(User::class),
        'name' => $faker->sentence,
        'instructions' => $faker->sentence(100)
    ];
});
